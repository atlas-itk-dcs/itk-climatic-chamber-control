import sys
# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, '')

import edgetech

import logging
import csv
import time
logger = logging.getLogger(__name__)

# Get a dew master object, this will print the status of the DewMaster
n = 0
if dm:
    print('the instance exists')
csv_file_path = 'dew_point_data.csv'
n=0
with open(csv_file_path, mode='w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['Timestamp', 'ADC Value', 'Temperature'])  # Header row

    try:
        while n < 5:
            # Read a line from the serial port
            dm = edgetech.DewMaster("/dev/serial/by-id/usb-FTDI_USB__-__Serial_Cable_FT8CU93A-if03-port0")
            dp_rh_values = dm.get_data_immediate(True)
            dp = dp_rh_values[0]
            rh = dp_rh_values[1]

            # Get the current timestamp
            timestamp = time.strftime('%Y-%m-%d %H:%M:%S')

            # Write the timestamp, ADC value, and temperature to the CSV file
            csv_writer.writerow([timestamp, dp, rh])
            print(f'{timestamp}, {dp}, {rh}')
            n = n+1
    except KeyboardInterrupt:
        print('Stopping data collection.')


