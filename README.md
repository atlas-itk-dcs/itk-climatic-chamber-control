# itk-climatic-chamber-control

## Collaborate with your team
- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)


## Description
- This project aims for automatically controling the temperature (T) and Relative humidity (RH) inside the climatic chamber in the lab 168/R-G14
- The auto control will help doing calibration of FOS sensors much easier

## Project status
- Current setup in `Chammber_script.py` uses:
    - Readout RH from Dew Master
    - Control RH from valves of dry air and moist H20

## TODO:
- Integrate the temperature sensor
- Integrate the Chiller
- Design the whole calibration processes for:
    - T test
    - RH test
